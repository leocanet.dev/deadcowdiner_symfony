<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index/index.html.twig */
class __TwigTemplate_53bfe11530fa08bfdbe45822a6b2685f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "index/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "DeadCowDiner - Index";
    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo twig_include($this->env, $context, "header/header.html.twig");
        echo "
     <main>
      <div class=\"top_index\">
        <div id=\"currently\">
          <div id=\"open\"></div>
          <p id=\"currently_open\">Restaurant ouvert</p>
        </div>

        <a href=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_reservation_new");
        echo "\"><img  src=\"/images/reserver.png\" alt=\"reservation\" class=\"input_reservation\"></img></a>
      </div>

      <div id=\"demo\" class=\"carousel slide\" data-bs-ride=\"carousel\">
        <div class=\"carousel-indicators\">
          <button
            type=\"button\"
            data-bs-target=\"#demo\"
            data-bs-slide-to=\"0\"
            class=\"active\"
          ></button>
          <button
            type=\"button\"
            data-bs-target=\"#demo\"
            data-bs-slide-to=\"1\"
          ></button>
          <button
            type=\"button\"
            data-bs-target=\"#demo\"
            data-bs-slide-to=\"2\"
          ></button>
        </div>

        <div class=\"carousel-inner\">
          <div class=\"carousel-item active\">
            <img
              src=\"./images/burger1.jpg\"
              alt=\"burger\"
              class=\"d-block w-100\"
            />
          </div>
          <div class=\"carousel-item\">
            <img
              src=\"./images/burger2.jpg\"
              alt=\"burger2\"
              class=\"d-block w-100\"
            />
          </div>
          <div class=\"carousel-item\">
            <img
              src=\"./images/burger3.jpg\"
              alt=\"burger3\"
              class=\"d-block w-100\"
            />
          </div>
        </div>

        <button
          class=\"carousel-control-prev\"
          type=\"button\"
          data-bs-target=\"#demo\"
          data-bs-slide=\"prev\"
        >
          <span class=\"carousel-control-prev-icon\"></span>
        </button>
        <button
          class=\"carousel-control-next\"
          type=\"button\"
          data-bs-target=\"#demo\"
          data-bs-slide=\"next\"
        >
          <span class=\"carousel-control-next-icon\"></span>
        </button>
      </div>
    </main>
    ";
        // line 80
        echo twig_include($this->env, $context, "footer/footer.html.twig");
        echo "
    <script src=\"/js/mainindex.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "index/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 80,  69 => 15,  58 => 7,  54 => 6,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "index/index.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/index/index.html.twig");
    }
}
