<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* header/header.html.twig */
class __TwigTemplate_f2b2a73e33820bf99a9b739c3188a627 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "header/header.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "
    <header>
      <div class=\"wrapper\">
      <a href=\"/\"><img class=\"logo\" src=\"/images/logo_small.png\" alt=\"logo\" width=\"250px\"center cover/></a>
      </div>
      <div class=\"wrapper\" id=\"title\"><h1>Dead Cow Diner</h1></div>
      <a class=\"icon\" onclick=\"myFunction()\">&#9776;</a>
    </header>
    <nav id=\"navbar\" class=\"nav\">
      <ul>
        <li><a class=\"ms-2\" href=\"/contenu\">Présentation</a></li>
        <li><a href=\"/carte\">Carte</a></li>
        <li><a href=\"/coordonnee\">Coordonnées</a></li>
        <li><a href=\"/contact\">Avis</a></li>
      </ul>
    </nav>

";
    }

    public function getTemplateName()
    {
        return "header/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "header/header.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/header/header.html.twig");
    }
}
