<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* reservation/_form.html.twig */
class __TwigTemplate_19437a3f9a2cc06dc190876b72dd456d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 4
        echo "
<div class=\"container mb-5 mt-5 d-flex flex-column justify-content-center\" id=\"form-contact\">
    <div class=\"container-full text-center\">
    </div>
    <div class=\"container box-form-contact\">
        ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
    </div>


    <div class=\"container-full d-flex justify-content-center\">
        <button class=\"btn btn-dark mt-4 mb-4\">Envoyer</button>
    </div>
    
    ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
</div>

";
    }

    // line 1
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "<link rel=\"stylesheet\" href=\"/styles/contact.css\">
";
    }

    public function getTemplateName()
    {
        return "reservation/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 2,  70 => 1,  62 => 18,  51 => 10,  47 => 9,  40 => 4,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "reservation/_form.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/reservation/_form.html.twig");
    }
}
