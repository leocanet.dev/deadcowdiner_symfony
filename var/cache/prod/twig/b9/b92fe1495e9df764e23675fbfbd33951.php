<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* produit/show.html.twig */
class __TwigTemplate_cecc8dc7cab425c11f7f08efd3709013 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "produit/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["produit"] ?? null), "nom", [], "any", false, false, false, 3), "html", null, true);
    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "<link rel=\"stylesheet\" href=\"/styles/carte.css\">
";
    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        echo twig_include($this->env, $context, "header/header.html.twig");
        echo "

     <section class=\"section-products d-flex align-items-center\">
\t\t<div id=\"show-produit\" class=\"container d-flex align-items-center justify-content-center\">
\t\t\t<div class=\"row justify-content-center text-center ms-5\">
\t\t\t</div>
            ";
        // line 17
        echo "                <div class=\"col-md-6 col-lg-4 col-xl-3\">
\t\t\t\t\t<div id=\"product-";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["produit"] ?? null), "id", [], "any", false, false, false, 18), "html", null, true);
        echo "\" class=\"single-product\">
\t\t\t\t\t\t<div class=\"part-1\" style=\"background: url(";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["produit"] ?? null), "image", [], "any", false, false, false, 19), "html", null, true);
        echo ") no-repeat center; background-size: cover; transition: all 0.3s; width: 300px;\";>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"part-2\">
\t\t\t\t\t\t\t<h2 class=\"product-title\">";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["produit"] ?? null), "nom", [], "any", false, false, false, 22), "html", null, true);
        echo "</h2>
\t\t\t\t\t\t\t<h3 class=\"product-price\">";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["produit"] ?? null), "prix", [], "any", false, false, false, 23), "html", null, true);
        echo " €</h3>
                            <h4 class=\"product-dispo\">Disponibilité : ";
        // line 24
        echo ((twig_get_attribute($this->env, $this->source, ($context["produit"] ?? null), "disponibilite", [], "any", false, false, false, 24)) ? ("&#128994") : ("&#128992"));
        echo "</h4>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
            <div class=\"wrapper-descr\">
                <div class=\"container-fluid text-center\">
                    <h5 class=\"mt-1\">";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["produit"] ?? null), "nom", [], "any", false, false, false, 30), "html", null, true);
        echo "</h5>
                </div>
                <div class=\"container\">
                    <p>";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["produit"] ?? null), "description", [], "any", false, false, false, 33), "html", null, true);
        echo "</p>
                </div>
                
            </div>
\t\t</div>
    </section>
    ";
        // line 39
        echo twig_include($this->env, $context, "footer/footer.html.twig");
        echo "
";
    }

    public function getTemplateName()
    {
        return "produit/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 39,  114 => 33,  108 => 30,  99 => 24,  95 => 23,  91 => 22,  85 => 19,  81 => 18,  78 => 17,  68 => 10,  64 => 9,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "produit/show.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/produit/show.html.twig");
    }
}
