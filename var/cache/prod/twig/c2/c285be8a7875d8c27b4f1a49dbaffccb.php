<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* header_admin/header_admin.html.twig */
class __TwigTemplate_0fa9127ce46f95541ec228f94d50716f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "header_admin/header_admin.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "<link rel=\"stylesheet\" href=\"/styles/header_admin.css\">
";
    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
<header>
    <div class=\"box-redirect\">
            <a class=\"redirect\" href=\"/carte/gestion_produit\">Gestion de produits</a>
            <a class=\"redirect\" href=\"/reservation/gestion_reservation\">Gestion des réservations</a>
            <a class=\"redirect\" href=\"/contenu/gestion_contenu\">Gestion des contenus</a>
    </div>

    <div class=\"container-fluid d-flex align-items-center justify-content-center\">
        <p class=\"title\"><a class=\"redirect-index\" href=\"/\">Dead Cow Diner</a> | Back office</p>
        <a class=\"btn btn-danger justify-self-end ms-5\" href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
        echo "\">Déconnexion</a>
    </div>
    
</header>

";
    }

    public function getTemplateName()
    {
        return "header_admin/header_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 18,  60 => 8,  56 => 7,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "header_admin/header_admin.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/header_admin/header_admin.html.twig");
    }
}
