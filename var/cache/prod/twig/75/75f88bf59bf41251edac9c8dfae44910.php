<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contenu/index_admin.html.twig */
class __TwigTemplate_80b846e86cd156970607168cb6af14d4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "contenu/index_admin.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Contenu index";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo twig_include($this->env, $context, "header_admin/header_admin.html.twig");
        echo "

    <div class=\"container-full text-center\">
        <h2 class=\"mt-4\">Contenu index</h2>
    </div>

    <div class=\"container\">
        <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Text</th>
                <th>Image</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["contenus"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["contenu"]) {
            // line 24
            echo "            <tr>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contenu"], "id", [], "any", false, false, false, 25), "html", null, true);
            echo "</td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contenu"], "text", [], "any", false, false, false, 26), "html", null, true);
            echo "</td>
                <td><img src=\"";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contenu"], "image", [], "any", false, false, false, 27), "html", null, true);
            echo "\" alt=\"photo du contenu\" width=\"300px\" height=\"200px\"></td>
                <td>
                    <a class=\"btn btn-info btn-sm\" href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_contenu_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["contenu"], "id", [], "any", false, false, false, 29)]), "html", null, true);
            echo "\">Éditer</a>
                    <a class=\"btn btn-danger btn-sm mt-2\" href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_contenu_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["contenu"], "id", [], "any", false, false, false, 30)]), "html", null, true);
            echo "\">Supprimer</a>
                </td>
            </tr>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 34
            echo "            <tr>
                <td colspan=\"4\">no records found</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contenu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "        </tbody>
        </table>
    </div>

        <div class=\"container-full d-flex justify-content-center\">
             <a class=\"btn btn-dark mb-4\" href=\"";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_contenu_new");
        echo "\">Nouveau contenu</a>
        </div>
    ";
        // line 45
        echo twig_include($this->env, $context, "footer/footer.html.twig");
        echo "
";
    }

    public function getTemplateName()
    {
        return "contenu/index_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 45,  128 => 43,  121 => 38,  112 => 34,  103 => 30,  99 => 29,  94 => 27,  90 => 26,  86 => 25,  83 => 24,  78 => 23,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "contenu/index_admin.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/contenu/index_admin.html.twig");
    }
}
