<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contenu/index.html.twig */
class __TwigTemplate_6b8771ec7fbbf9ce4eb1ed7eb54006bd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "contenu/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Présentation";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "<link rel=\"stylesheet\" href=\"/styles/histoire.css\">
";
    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        echo twig_include($this->env, $context, "header/header.html.twig");
        echo "

        <div id=\"demo\" class=\"carousel slide\" data-bs-ride=\"carousel\">
            <div class=\"carousel-indicators\">
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"0\" class=\"active\"></button>
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"1\"></button>
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"2\"></button>
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"3\"></button>
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"4\"></button>

            </div>


            <div class=\"carousel-inner\">
                <div class=\"carousel-item active\">
                    <img src=\"https://lexperienceburger.com/wp-content/uploads/2020/05/Photo-Restaurant-Expe%CC%81rience-Burger-Be%CC%81thune-8.jpg\"
                        alt=\"burger\" class=\"d-block w-100\" height=\"300px\"/>
                </div>

                <div class=\"carousel-item\">
                    <img src=\"https://lexperienceburger.com/wp-content/uploads/2020/05/Photo-Restaurant-Expe%CC%81rience-Burger-Be%CC%81thune-12.jpg\"
                        alt=\"burger2\" class=\"d-block w-100\" height=\"300px\"/>
                </div>
                <div class=\"carousel-item\">
                    <img src=\"https://static.actu.fr/uploads/2020/06/restaurant-960x640.jpeg\" alt=\"burger3\"
                        class=\"d-block w-100\" height=\"300px\"/>
                </div>
                <div class=\"carousel-item\">
                    <img src=\"https://lexperienceburger.com/wp-content/uploads/2020/04/Viande.jpg\" alt=\"burger2\"
                        class=\"d-block w-100\" height=\"300px\"/>
                </div>
                <div class=\"carousel-item\">
                    <img src=\"https://image.freepik.com/photos-gratuite/burger-cotlet-boeuf-sauce-planche-bois_114579-2600.jpg\"
                        alt=\"burger2\" class=\"d-block w-100\" height=\"300px\"/>
                </div>
            </div>

            <button class=\"carousel-control-prev\" type=\"button\" data-bs-target=\"#demo\" data-bs-slide=\"prev\">
                <span class=\"carousel-control-prev-icon\"></span>
            </button>
            <button class=\"carousel-control-next\" type=\"button\" data-bs-target=\"#demo\" data-bs-slide=\"next\">
                <span class=\"carousel-control-next-icon\"></span>
            </button>
        </div>
        

        <div class=\"row m-4 mb-5\">

                <img class=\"img-content-";
        // line 58
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_0 = ($context["contenus"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null), "id", [], "any", false, false, false, 58), "html", null, true);
        echo " col img-content\" src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_1 = ($context["contenus"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[0] ?? null) : null), "image", [], "any", false, false, false, 58), "html", null, true);
        echo "\" alt=\"image du restaurant\">
                <div class=\"box-txt-content col-lg-3\">
                    <p class=\"txt-content\">";
        // line 60
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_2 = ($context["contenus"] ?? null)) && is_array($__internal_compile_2) || $__internal_compile_2 instanceof ArrayAccess ? ($__internal_compile_2[0] ?? null) : null), "text", [], "any", false, false, false, 60), "html", null, true);
        echo "</p>
                </div>

                <img class=\"img-content-";
        // line 63
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_3 = ($context["contenus"] ?? null)) && is_array($__internal_compile_3) || $__internal_compile_3 instanceof ArrayAccess ? ($__internal_compile_3[1] ?? null) : null), "id", [], "any", false, false, false, 63), "html", null, true);
        echo " col img-content\" src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_4 = ($context["contenus"] ?? null)) && is_array($__internal_compile_4) || $__internal_compile_4 instanceof ArrayAccess ? ($__internal_compile_4[1] ?? null) : null), "image", [], "any", false, false, false, 63), "html", null, true);
        echo "\" alt=\"image du restaurant\">
                <div class=\"box-txt-content col-lg-3\">
                    <p class=\"txt-content\">";
        // line 65
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_5 = ($context["contenus"] ?? null)) && is_array($__internal_compile_5) || $__internal_compile_5 instanceof ArrayAccess ? ($__internal_compile_5[1] ?? null) : null), "text", [], "any", false, false, false, 65), "html", null, true);
        echo "</p>
                </div>

                <img class=\"img-content-";
        // line 68
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_6 = ($context["contenus"] ?? null)) && is_array($__internal_compile_6) || $__internal_compile_6 instanceof ArrayAccess ? ($__internal_compile_6[2] ?? null) : null), "id", [], "any", false, false, false, 68), "html", null, true);
        echo " col img-content\" src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_7 = ($context["contenus"] ?? null)) && is_array($__internal_compile_7) || $__internal_compile_7 instanceof ArrayAccess ? ($__internal_compile_7[2] ?? null) : null), "image", [], "any", false, false, false, 68), "html", null, true);
        echo "\" alt=\"image du restaurant\">
                <div class=\"box-txt-content col-lg-3\">
                    <p class=\"txt-content\">";
        // line 70
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_8 = ($context["contenus"] ?? null)) && is_array($__internal_compile_8) || $__internal_compile_8 instanceof ArrayAccess ? ($__internal_compile_8[2] ?? null) : null), "text", [], "any", false, false, false, 70), "html", null, true);
        echo "</p>
                </div>
                
                <div class=\"box-txt-content col-lg-5\">
                    <p class=\"txt-content\">";
        // line 74
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_9 = ($context["contenus"] ?? null)) && is_array($__internal_compile_9) || $__internal_compile_9 instanceof ArrayAccess ? ($__internal_compile_9[3] ?? null) : null), "text", [], "any", false, false, false, 74), "html", null, true);
        echo "</p>
                </div>
                <img class=\"img-content-";
        // line 76
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_10 = ($context["contenus"] ?? null)) && is_array($__internal_compile_10) || $__internal_compile_10 instanceof ArrayAccess ? ($__internal_compile_10[3] ?? null) : null), "id", [], "any", false, false, false, 76), "html", null, true);
        echo " col-lg-7 img-content\" src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_11 = ($context["contenus"] ?? null)) && is_array($__internal_compile_11) || $__internal_compile_11 instanceof ArrayAccess ? ($__internal_compile_11[3] ?? null) : null), "image", [], "any", false, false, false, 76), "html", null, true);
        echo "\" alt=\"image du restaurant\">

                <img class=\"img-content-";
        // line 78
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_12 = ($context["contenus"] ?? null)) && is_array($__internal_compile_12) || $__internal_compile_12 instanceof ArrayAccess ? ($__internal_compile_12[4] ?? null) : null), "id", [], "any", false, false, false, 78), "html", null, true);
        echo " col img-content\" src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_13 = ($context["contenus"] ?? null)) && is_array($__internal_compile_13) || $__internal_compile_13 instanceof ArrayAccess ? ($__internal_compile_13[4] ?? null) : null), "image", [], "any", false, false, false, 78), "html", null, true);
        echo "\" alt=\"image du restaurant\">
                <div class=\"box-txt-content col-lg-5\">
                    <p class=\"txt-content\">";
        // line 80
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_compile_14 = ($context["contenus"] ?? null)) && is_array($__internal_compile_14) || $__internal_compile_14 instanceof ArrayAccess ? ($__internal_compile_14[4] ?? null) : null), "text", [], "any", false, false, false, 80), "html", null, true);
        echo "</p>
                </div>

        </div>
    ";
        // line 84
        echo twig_include($this->env, $context, "footer/footer.html.twig");
        echo "
";
    }

    public function getTemplateName()
    {
        return "contenu/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 84,  179 => 80,  172 => 78,  165 => 76,  160 => 74,  153 => 70,  146 => 68,  140 => 65,  133 => 63,  127 => 60,  120 => 58,  68 => 10,  64 => 9,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "contenu/index.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/contenu/index.html.twig");
    }
}
