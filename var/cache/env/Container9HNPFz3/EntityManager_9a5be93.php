<?php

namespace Container9HNPFz3;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder44c54 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer79b0d = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesc580b = [
        
    ];

    public function getConnection()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getConnection', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getMetadataFactory', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getExpressionBuilder', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'beginTransaction', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getCache', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getCache();
    }

    public function transactional($func)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'transactional', array('func' => $func), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'wrapInTransaction', array('func' => $func), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'commit', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->commit();
    }

    public function rollback()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'rollback', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getClassMetadata', array('className' => $className), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'createQuery', array('dql' => $dql), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'createNamedQuery', array('name' => $name), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'createQueryBuilder', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'flush', array('entity' => $entity), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'clear', array('entityName' => $entityName), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->clear($entityName);
    }

    public function close()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'close', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->close();
    }

    public function persist($entity)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'persist', array('entity' => $entity), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'remove', array('entity' => $entity), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'refresh', array('entity' => $entity), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'detach', array('entity' => $entity), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'merge', array('entity' => $entity), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getRepository', array('entityName' => $entityName), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'contains', array('entity' => $entity), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getEventManager', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getConfiguration', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'isOpen', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getUnitOfWork', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getProxyFactory', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'initializeObject', array('obj' => $obj), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'getFilters', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'isFiltersStateClean', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'hasFilters', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return $this->valueHolder44c54->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer79b0d = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder44c54) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder44c54 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder44c54->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, '__get', ['name' => $name], $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        if (isset(self::$publicPropertiesc580b[$name])) {
            return $this->valueHolder44c54->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder44c54;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder44c54;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder44c54;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder44c54;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, '__isset', array('name' => $name), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder44c54;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder44c54;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, '__unset', array('name' => $name), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder44c54;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder44c54;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, '__clone', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        $this->valueHolder44c54 = clone $this->valueHolder44c54;
    }

    public function __sleep()
    {
        $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, '__sleep', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;

        return array('valueHolder44c54');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer79b0d = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer79b0d;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer79b0d && ($this->initializer79b0d->__invoke($valueHolder44c54, $this, 'initializeProxy', array(), $this->initializer79b0d) || 1) && $this->valueHolder44c54 = $valueHolder44c54;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder44c54;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder44c54;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
