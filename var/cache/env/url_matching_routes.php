<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/contact' => [[['_route' => 'app_contact', '_controller' => 'App\\Controller\\ContactController::index'], null, null, null, false, false, null]],
        '/contenu' => [[['_route' => 'app_contenu_index', '_controller' => 'App\\Controller\\ContenuController::index'], null, ['GET' => 0], null, true, false, null]],
        '/contenu/gestion_contenu' => [[['_route' => 'app_contenu_index_admin', '_controller' => 'App\\Controller\\ContenuController::indexAdmin'], null, ['GET' => 0], null, false, false, null]],
        '/contenu/new' => [[['_route' => 'app_contenu_new', '_controller' => 'App\\Controller\\ContenuController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/coordonnee' => [[['_route' => 'app_coordonnee', '_controller' => 'App\\Controller\\CoordonneeController::index'], null, null, null, false, false, null]],
        '/footer' => [[['_route' => 'app_footer', '_controller' => 'App\\Controller\\FooterController::index'], null, null, null, false, false, null]],
        '/header/admin' => [[['_route' => 'app_header_admin', '_controller' => 'App\\Controller\\HeaderAdminController::index'], null, null, null, false, false, null]],
        '/header' => [[['_route' => 'app_header', '_controller' => 'App\\Controller\\HeaderController::index'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'app_index', '_controller' => 'App\\Controller\\IndexController::index'], null, null, null, false, false, null]],
        '/carte' => [[['_route' => 'app_produit_index', '_controller' => 'App\\Controller\\ProduitController::index'], null, ['GET' => 0], null, true, false, null]],
        '/carte/gestion_produit' => [[['_route' => 'app_produit_index_admin', '_controller' => 'App\\Controller\\ProduitController::index_admin'], null, ['GET' => 0], null, false, false, null]],
        '/carte/new' => [[['_route' => 'app_produit_new', '_controller' => 'App\\Controller\\ProduitController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/register' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\RegistrationController::register'], null, null, null, false, false, null]],
        '/reservation' => [[['_route' => 'app_reservation_index', '_controller' => 'App\\Controller\\ReservationController::index'], null, ['GET' => 0], null, true, false, null]],
        '/reservation/gestion_reservation' => [[['_route' => 'app_reservation_index_admin', '_controller' => 'App\\Controller\\ReservationController::index_admin'], null, ['GET' => 0], null, false, false, null]],
        '/reservation/new' => [[['_route' => 'app_reservation_new', '_controller' => 'App\\Controller\\ReservationController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/c(?'
                    .'|ontenu/([^/]++)(?'
                        .'|/edit(*:35)'
                        .'|(*:42)'
                    .')'
                    .'|arte/([^/]++)(?'
                        .'|(*:66)'
                        .'|/edit(*:78)'
                        .'|(*:85)'
                    .')'
                .')'
                .'|/reservation/([^/]++)(?'
                    .'|(*:118)'
                    .'|/edit(*:131)'
                    .'|(*:139)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        35 => [[['_route' => 'app_contenu_edit', '_controller' => 'App\\Controller\\ContenuController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        42 => [[['_route' => 'app_contenu_delete', '_controller' => 'App\\Controller\\ContenuController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        66 => [[['_route' => 'app_produit_show', '_controller' => 'App\\Controller\\ProduitController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        78 => [[['_route' => 'app_produit_edit', '_controller' => 'App\\Controller\\ProduitController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        85 => [[['_route' => 'app_produit_delete', '_controller' => 'App\\Controller\\ProduitController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        118 => [[['_route' => 'app_reservation_show', '_controller' => 'App\\Controller\\ReservationController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        131 => [[['_route' => 'app_reservation_edit', '_controller' => 'App\\Controller\\ReservationController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        139 => [
            [['_route' => 'app_reservation_delete', '_controller' => 'App\\Controller\\ReservationController::delete'], ['id'], ['POST' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
