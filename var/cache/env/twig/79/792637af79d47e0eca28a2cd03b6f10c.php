<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* produit/edit.html.twig */
class __TwigTemplate_a949051ef3d7cd158fbb39ad5e70b726 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "produit/edit.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "produit/edit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Éditer un produit";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        echo "<style>
.wrapper-edit{
    height: 100%;
    margin-bottom: 4rem;
}
</style>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 14
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    ";
        echo twig_include($this->env, $context, "header_admin/header_admin.html.twig");
        echo "

    <main>
        <div class=\"wrapper-edit\">
         <div class=\"container-full text-center\">
            <h2 class=\"mt-4\">Éditer - ";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["produit"]) || array_key_exists("produit", $context) ? $context["produit"] : (function () { throw new RuntimeError('Variable "produit" does not exist.', 20, $this->source); })()), "nom", [], "any", false, false, false, 20), "html", null, true);
        echo "</h2>
        </div>
        ";
        // line 22
        echo twig_include($this->env, $context, "produit/_form.html.twig", ["button_label" => "Sauvegarder"]);
        echo "

        <div class=\"d-flex\">
            <a class=\"btn btn-primary me-2 mt-2\" href=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_produit_index_admin");
        echo "\">Retour</a>
            ";
        // line 26
        echo twig_include($this->env, $context, "produit/_delete_form.html.twig");
        echo "
        </div>
        </div>
    </main>

    ";
        // line 31
        echo twig_include($this->env, $context, "footer/footer.html.twig");
        echo "

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 35
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 36
        echo "<script>
window.onload = () => {
    let deleteBtn = document.querySelector('#delete-prod');
    deleteBtn.classList.add('btn', 'btn-danger', 'mt-2');
    deleteBtn.textContent = 'Supprimer';
    deleteBtn.style.fontSize = '16px';
    deleteBtn.classList.remove('p-0');
}
</script>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "produit/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 36,  137 => 35,  127 => 31,  119 => 26,  115 => 25,  109 => 22,  104 => 20,  95 => 15,  88 => 14,  75 => 6,  68 => 5,  55 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Éditer un produit{% endblock %}

{% block stylesheets %}
<style>
.wrapper-edit{
    height: 100%;
    margin-bottom: 4rem;
}
</style>
{% endblock %}

{% block body %}
    {{ include('header_admin/header_admin.html.twig') }}

    <main>
        <div class=\"wrapper-edit\">
         <div class=\"container-full text-center\">
            <h2 class=\"mt-4\">Éditer - {{ produit.nom }}</h2>
        </div>
        {{ include('produit/_form.html.twig', {'button_label': 'Sauvegarder'}) }}

        <div class=\"d-flex\">
            <a class=\"btn btn-primary me-2 mt-2\" href=\"{{ path('app_produit_index_admin') }}\">Retour</a>
            {{ include('produit/_delete_form.html.twig') }}
        </div>
        </div>
    </main>

    {{ include('footer/footer.html.twig') }}

{% endblock %}

{% block javascripts %}
<script>
window.onload = () => {
    let deleteBtn = document.querySelector('#delete-prod');
    deleteBtn.classList.add('btn', 'btn-danger', 'mt-2');
    deleteBtn.textContent = 'Supprimer';
    deleteBtn.style.fontSize = '16px';
    deleteBtn.classList.remove('p-0');
}
</script>
{% endblock %}", "produit/edit.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/produit/edit.html.twig");
    }
}
