<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index/index.html.twig */
class __TwigTemplate_cc98d3d1f5af049ee0d03cca2c91e2cb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "index/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "DeadCowDiner - Index";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        echo "<link rel=\"stylesheet\" href=\"/styles/carousel.css\">
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick.min.css\">
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick-theme.min.css\">
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 11
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo twig_include($this->env, $context, "header/header.html.twig");
        echo "
     <main>
      <div class=\"top_index\">
        <div id=\"currently\">
          <div id=\"open\"></div>
          <p id=\"currently_open\">Restaurant ouvert</p>
        </div>

        <a href=\"";
        // line 20
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_reservation_new");
        echo "\"><img  src=\"/images/reserver.png\" alt=\"reservation\" class=\"input_reservation\"></img></a>
      </div>

        <div class=\"wrapper-carousel mt-5\">
            <div class=\"carousel\">
                <div><img class=\"img-carousel\" src=\"/images/burger1.jpg\" width=\"500\" height=\"300\"></div>
                <div><img class=\"img-carousel\" src=\"/images/burger2.jpg\" width=\"500\" height=\"300\"></div>
                <div><img class=\"img-carousel\" src=\"/images/burger3.jpg\" width=\"500\" height=\"300\"></div>
                <div><img class=\"img-carousel\" src=\"/images/burger4.png\" width=\"500\" height=\"300\"></div>
                <div><img class=\"img-carousel\" src=\"/images/burger5.png\" width=\"500\" height=\"300\"></div>
                <div><img class=\"img-carousel\" src=\"/images/burger6.png\" width=\"500\" height=\"300\"></div>
            </div>
        </div>
    </main>
    ";
        // line 34
        echo twig_include($this->env, $context, "footer/footer.html.twig");
        echo "
    <script src=\"/js/mainindex.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick.min.js\"></script>
    <script src=\"/js/carousel.js\"></script>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "index/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 34,  102 => 20,  91 => 12,  84 => 11,  74 => 6,  67 => 5,  54 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}DeadCowDiner - Index{% endblock %}

{% block stylesheets %}
<link rel=\"stylesheet\" href=\"/styles/carousel.css\">
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick.min.css\">
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick-theme.min.css\">
{% endblock %}

{% block body %}
{{ include('header/header.html.twig') }}
     <main>
      <div class=\"top_index\">
        <div id=\"currently\">
          <div id=\"open\"></div>
          <p id=\"currently_open\">Restaurant ouvert</p>
        </div>

        <a href=\"{{ path('app_reservation_new') }}\"><img  src=\"/images/reserver.png\" alt=\"reservation\" class=\"input_reservation\"></img></a>
      </div>

        <div class=\"wrapper-carousel mt-5\">
            <div class=\"carousel\">
                <div><img class=\"img-carousel\" src=\"/images/burger1.jpg\" width=\"500\" height=\"300\"></div>
                <div><img class=\"img-carousel\" src=\"/images/burger2.jpg\" width=\"500\" height=\"300\"></div>
                <div><img class=\"img-carousel\" src=\"/images/burger3.jpg\" width=\"500\" height=\"300\"></div>
                <div><img class=\"img-carousel\" src=\"/images/burger4.png\" width=\"500\" height=\"300\"></div>
                <div><img class=\"img-carousel\" src=\"/images/burger5.png\" width=\"500\" height=\"300\"></div>
                <div><img class=\"img-carousel\" src=\"/images/burger6.png\" width=\"500\" height=\"300\"></div>
            </div>
        </div>
    </main>
    {{ include('footer/footer.html.twig') }}
    <script src=\"/js/mainindex.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick.min.js\"></script>
    <script src=\"/js/carousel.js\"></script>
{% endblock %}
", "index/index.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/index/index.html.twig");
    }
}
