<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contenu/edit.html.twig */
class __TwigTemplate_37366e207dd16cf757387aff4def64b1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contenu/edit.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "contenu/edit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Éditer";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        echo "<style>
.wrapper-edit{
    height: 100%;
    margin-bottom: 4rem;
}
#wrapper-title{
    background-color: rgb(0 0 0 / 14%);
}
</style>
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 17
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 18
        echo twig_include($this->env, $context, "header_admin/header_admin.html.twig");
        echo "

    <div class=\"wrapper-edit\">
        <div  id=\"wrapper-title\" class=\"container-full text-center\">
            <h2 class=\"mt-4\">Éditer un contenu</h2>
        </div>

        <div class=\"container\">
            ";
        // line 26
        echo twig_include($this->env, $context, "contenu/_form.html.twig", ["button_label" => "Valider"]);
        echo "
        </div>
        
        <div class=\"container\">
            <a class=\"btn btn-primary mt-2\" href=\"";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_contenu_index_admin");
        echo "\">Retour</a>
            ";
        // line 31
        echo twig_include($this->env, $context, "contenu/_delete_form.html.twig");
        echo "
        </div>
    </div>




    ";
        // line 38
        echo twig_include($this->env, $context, "footer/footer.html.twig");
        echo "
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "contenu/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 38,  119 => 31,  115 => 30,  108 => 26,  97 => 18,  90 => 17,  74 => 6,  67 => 5,  54 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Éditer{% endblock %}

{% block stylesheets %}
<style>
.wrapper-edit{
    height: 100%;
    margin-bottom: 4rem;
}
#wrapper-title{
    background-color: rgb(0 0 0 / 14%);
}
</style>
{% endblock %}

{% block body %}
{{ include('header_admin/header_admin.html.twig') }}

    <div class=\"wrapper-edit\">
        <div  id=\"wrapper-title\" class=\"container-full text-center\">
            <h2 class=\"mt-4\">Éditer un contenu</h2>
        </div>

        <div class=\"container\">
            {{ include('contenu/_form.html.twig', {'button_label': 'Valider'}) }}
        </div>
        
        <div class=\"container\">
            <a class=\"btn btn-primary mt-2\" href=\"{{ path('app_contenu_index_admin') }}\">Retour</a>
            {{ include('contenu/_delete_form.html.twig') }}
        </div>
    </div>




    {{ include('footer/footer.html.twig') }}
{% endblock %}
", "contenu/edit.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/contenu/edit.html.twig");
    }
}
