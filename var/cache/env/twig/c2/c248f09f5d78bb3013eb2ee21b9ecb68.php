<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* header/header.html.twig */
class __TwigTemplate_43bd33e99b5ae3a7c40720d962e3f70a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "header/header.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "header/header.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <header>
      <div class=\"wrapper\">
      <a href=\"/\"><img class=\"logo\" src=\"/images/logo_small.png\" alt=\"logo\" width=\"250px\"center cover/></a>
      </div>
      <div class=\"wrapper\" id=\"title\"><h1>Dead Cow Diner</h1></div>
      <a class=\"icon\" onclick=\"getNavResponsive()\">&#9776;</a>
    </header>
    <nav id=\"navbar\" class=\"nav nav-sticky\">
      <ul>
        <li><a class=\"ms-2\" href=\"/contenu\">Présentation</a></li>
        <li><a href=\"/carte\">Carte</a></li>
        <li><a href=\"/coordonnee\">Coordonnées</a></li>
        <li><a href=\"/contact\">Avis</a></li>
      </ul>
    </nav>

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "header/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 4,  52 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

    <header>
      <div class=\"wrapper\">
      <a href=\"/\"><img class=\"logo\" src=\"/images/logo_small.png\" alt=\"logo\" width=\"250px\"center cover/></a>
      </div>
      <div class=\"wrapper\" id=\"title\"><h1>Dead Cow Diner</h1></div>
      <a class=\"icon\" onclick=\"getNavResponsive()\">&#9776;</a>
    </header>
    <nav id=\"navbar\" class=\"nav nav-sticky\">
      <ul>
        <li><a class=\"ms-2\" href=\"/contenu\">Présentation</a></li>
        <li><a href=\"/carte\">Carte</a></li>
        <li><a href=\"/coordonnee\">Coordonnées</a></li>
        <li><a href=\"/contact\">Avis</a></li>
      </ul>
    </nav>

{% endblock %}
", "header/header.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/header/header.html.twig");
    }
}
