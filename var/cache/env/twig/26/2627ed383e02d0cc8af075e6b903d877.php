<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contenu/index.html.twig */
class __TwigTemplate_823ca9f2aa40835be73a581ff800919a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contenu/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "contenu/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Présentation";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        echo "<link rel=\"stylesheet\" href=\"/styles/histoire.css\">
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "    ";
        echo twig_include($this->env, $context, "header/header.html.twig");
        echo "

        <div id=\"demo\" class=\"carousel slide\" data-bs-ride=\"carousel\">
            <div class=\"carousel-indicators\">
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"0\" class=\"active\"></button>
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"1\"></button>
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"2\"></button>
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"3\"></button>
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"4\"></button>
            </div>


            <div class=\"carousel-inner\">
                <div class=\"carousel-item active\">
                    <img src=\"https://lexperienceburger.com/wp-content/uploads/2020/05/Photo-Restaurant-Expe%CC%81rience-Burger-Be%CC%81thune-8.jpg\"
                        alt=\"burger\" class=\"d-block w-100\" height=\"300px\"/>
                </div>

                <div class=\"carousel-item\">
                    <img src=\"https://lexperienceburger.com/wp-content/uploads/2020/05/Photo-Restaurant-Expe%CC%81rience-Burger-Be%CC%81thune-12.jpg\"
                        alt=\"burger2\" class=\"d-block w-100\" height=\"300px\"/>
                </div>
                <div class=\"carousel-item\">
                    <img src=\"https://static.actu.fr/uploads/2020/06/restaurant-960x640.jpeg\" alt=\"burger3\"
                        class=\"d-block w-100\" height=\"300px\"/>
                </div>
                <div class=\"carousel-item\">
                    <img src=\"https://lexperienceburger.com/wp-content/uploads/2020/04/Viande.jpg\" alt=\"burger2\"
                        class=\"d-block w-100\" height=\"300px\"/>
                </div>
                <div class=\"carousel-item\">
                    <img src=\"https://image.freepik.com/photos-gratuite/burger-cotlet-boeuf-sauce-planche-bois_114579-2600.jpg\"
                        alt=\"burger2\" class=\"d-block w-100\" height=\"300px\"/>
                </div>
            </div>

            <button class=\"carousel-control-prev\" type=\"button\" data-bs-target=\"#demo\" data-bs-slide=\"prev\">
                <span class=\"carousel-control-prev-icon\"></span>
            </button>
            <button class=\"carousel-control-next\" type=\"button\" data-bs-target=\"#demo\" data-bs-slide=\"next\">
                <span class=\"carousel-control-next-icon\"></span>
            </button>
        </div>
        

        <div class=\"m-4 mb-5\">

            <div class=\"row\">
                <img class=\"img-content-";
        // line 58
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 58, $this->source); })()), 0, [], "array", false, false, false, 58), "id", [], "any", false, false, false, 58), "html", null, true);
        echo " img-content col-xs-12 col-sm-4\" src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 58, $this->source); })()), 0, [], "array", false, false, false, 58), "image", [], "any", false, false, false, 58), "html", null, true);
        echo "\" alt=\"image du restaurant\">
                <div class=\"box-txt-content col-xs-12 col-sm-4\">
                    <p class=\"txt-content\">";
        // line 60
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 60, $this->source); })()), 0, [], "array", false, false, false, 60), "text", [], "any", false, false, false, 60), "html", null, true);
        echo "</p>
                </div>
                <img class=\"img-content-";
        // line 62
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 62, $this->source); })()), 1, [], "array", false, false, false, 62), "id", [], "any", false, false, false, 62), "html", null, true);
        echo " img-content col-xs-12 col-sm-4\" src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 62, $this->source); })()), 1, [], "array", false, false, false, 62), "image", [], "any", false, false, false, 62), "html", null, true);
        echo "\" alt=\"image du restaurant\">
            </div>

            <div class=\"row\">
                <div class=\"box-txt-content col-xs-12 col-sm-6 col-md-3\">
                    <p class=\"txt-content\">";
        // line 67
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 67, $this->source); })()), 1, [], "array", false, false, false, 67), "text", [], "any", false, false, false, 67), "html", null, true);
        echo "</p>  
                </div>              
                    <img class=\"img-content-";
        // line 69
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 69, $this->source); })()), 2, [], "array", false, false, false, 69), "id", [], "any", false, false, false, 69), "html", null, true);
        echo " img-content col-xs-12 col-sm-12 col-md-6\" src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 69, $this->source); })()), 2, [], "array", false, false, false, 69), "image", [], "any", false, false, false, 69), "html", null, true);
        echo "\" alt=\"image du restaurant\">
                <div class=\"box-txt-content col-xs-12 col-sm-6 col-md-3\">
                    <p class=\"txt-content\">";
        // line 71
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 71, $this->source); })()), 2, [], "array", false, false, false, 71), "text", [], "any", false, false, false, 71), "html", null, true);
        echo "</p>
                </div>
            </div>

            <div class=\"row\">
                <div class=\"box-txt-content col-xs-12 col-sm-6 col-md-6\">
                    <p class=\"txt-content\">";
        // line 77
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 77, $this->source); })()), 3, [], "array", false, false, false, 77), "text", [], "any", false, false, false, 77), "html", null, true);
        echo "</p>
                </div>
                <img class=\"img-content-";
        // line 79
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 79, $this->source); })()), 3, [], "array", false, false, false, 79), "id", [], "any", false, false, false, 79), "html", null, true);
        echo " col-xs-12 col-sm-6 col-md-6 img-content\" src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 79, $this->source); })()), 3, [], "array", false, false, false, 79), "image", [], "any", false, false, false, 79), "html", null, true);
        echo "\" alt=\"image du restaurant\">
                <img class=\"img-content-";
        // line 80
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 80, $this->source); })()), 4, [], "array", false, false, false, 80), "id", [], "any", false, false, false, 80), "html", null, true);
        echo " col-xs-12 col-sm-12 col-md-6 img-content\" src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 80, $this->source); })()), 4, [], "array", false, false, false, 80), "image", [], "any", false, false, false, 80), "html", null, true);
        echo "\" alt=\"image du restaurant\">
                <div class=\"box-txt-content col-xs-12 col-sm-12 col-md-6\">
                    <p class=\"txt-content\">";
        // line 82
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 82, $this->source); })()), 4, [], "array", false, false, false, 82), "text", [], "any", false, false, false, 82), "html", null, true);
        echo "</p>
                </div>
            </div>
                
        </div>
    ";
        // line 87
        echo twig_include($this->env, $context, "footer/footer.html.twig");
        echo "
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "contenu/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  210 => 87,  202 => 82,  195 => 80,  189 => 79,  184 => 77,  175 => 71,  168 => 69,  163 => 67,  153 => 62,  148 => 60,  141 => 58,  89 => 10,  82 => 9,  74 => 6,  67 => 5,  54 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Présentation{% endblock %}

{% block stylesheets %}
<link rel=\"stylesheet\" href=\"/styles/histoire.css\">
{% endblock %}

{% block body %}
    {{ include('header/header.html.twig') }}

        <div id=\"demo\" class=\"carousel slide\" data-bs-ride=\"carousel\">
            <div class=\"carousel-indicators\">
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"0\" class=\"active\"></button>
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"1\"></button>
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"2\"></button>
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"3\"></button>
                <button type=\"button\" data-bs-target=\"#demo\" data-bs-slide-to=\"4\"></button>
            </div>


            <div class=\"carousel-inner\">
                <div class=\"carousel-item active\">
                    <img src=\"https://lexperienceburger.com/wp-content/uploads/2020/05/Photo-Restaurant-Expe%CC%81rience-Burger-Be%CC%81thune-8.jpg\"
                        alt=\"burger\" class=\"d-block w-100\" height=\"300px\"/>
                </div>

                <div class=\"carousel-item\">
                    <img src=\"https://lexperienceburger.com/wp-content/uploads/2020/05/Photo-Restaurant-Expe%CC%81rience-Burger-Be%CC%81thune-12.jpg\"
                        alt=\"burger2\" class=\"d-block w-100\" height=\"300px\"/>
                </div>
                <div class=\"carousel-item\">
                    <img src=\"https://static.actu.fr/uploads/2020/06/restaurant-960x640.jpeg\" alt=\"burger3\"
                        class=\"d-block w-100\" height=\"300px\"/>
                </div>
                <div class=\"carousel-item\">
                    <img src=\"https://lexperienceburger.com/wp-content/uploads/2020/04/Viande.jpg\" alt=\"burger2\"
                        class=\"d-block w-100\" height=\"300px\"/>
                </div>
                <div class=\"carousel-item\">
                    <img src=\"https://image.freepik.com/photos-gratuite/burger-cotlet-boeuf-sauce-planche-bois_114579-2600.jpg\"
                        alt=\"burger2\" class=\"d-block w-100\" height=\"300px\"/>
                </div>
            </div>

            <button class=\"carousel-control-prev\" type=\"button\" data-bs-target=\"#demo\" data-bs-slide=\"prev\">
                <span class=\"carousel-control-prev-icon\"></span>
            </button>
            <button class=\"carousel-control-next\" type=\"button\" data-bs-target=\"#demo\" data-bs-slide=\"next\">
                <span class=\"carousel-control-next-icon\"></span>
            </button>
        </div>
        

        <div class=\"m-4 mb-5\">

            <div class=\"row\">
                <img class=\"img-content-{{ contenus[0].id }} img-content col-xs-12 col-sm-4\" src=\"{{ contenus[0].image }}\" alt=\"image du restaurant\">
                <div class=\"box-txt-content col-xs-12 col-sm-4\">
                    <p class=\"txt-content\">{{ contenus[0].text }}</p>
                </div>
                <img class=\"img-content-{{ contenus[1].id }} img-content col-xs-12 col-sm-4\" src=\"{{ contenus[1].image }}\" alt=\"image du restaurant\">
            </div>

            <div class=\"row\">
                <div class=\"box-txt-content col-xs-12 col-sm-6 col-md-3\">
                    <p class=\"txt-content\">{{ contenus[1].text }}</p>  
                </div>              
                    <img class=\"img-content-{{ contenus[2].id }} img-content col-xs-12 col-sm-12 col-md-6\" src=\"{{ contenus[2].image }}\" alt=\"image du restaurant\">
                <div class=\"box-txt-content col-xs-12 col-sm-6 col-md-3\">
                    <p class=\"txt-content\">{{ contenus[2].text }}</p>
                </div>
            </div>

            <div class=\"row\">
                <div class=\"box-txt-content col-xs-12 col-sm-6 col-md-6\">
                    <p class=\"txt-content\">{{ contenus[3].text }}</p>
                </div>
                <img class=\"img-content-{{ contenus[3].id }} col-xs-12 col-sm-6 col-md-6 img-content\" src=\"{{ contenus[3].image }}\" alt=\"image du restaurant\">
                <img class=\"img-content-{{ contenus[4].id }} col-xs-12 col-sm-12 col-md-6 img-content\" src=\"{{ contenus[4].image }}\" alt=\"image du restaurant\">
                <div class=\"box-txt-content col-xs-12 col-sm-12 col-md-6\">
                    <p class=\"txt-content\">{{ contenus[4].text }}</p>
                </div>
            </div>
                
        </div>
    {{ include('footer/footer.html.twig') }}
{% endblock %}
", "contenu/index.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/contenu/index.html.twig");
    }
}
