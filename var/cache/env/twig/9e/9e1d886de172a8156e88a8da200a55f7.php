<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contenu/index_admin.html.twig */
class __TwigTemplate_b08e7bac7232131219f768ed75444ee3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contenu/index_admin.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "contenu/index_admin.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Contenu index";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo twig_include($this->env, $context, "header_admin/header_admin.html.twig");
        echo "

    <div class=\"container-full text-center\">
        <h2 class=\"mt-4\">Contenu index</h2>
    </div>

    <div class=\"container\">
        <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Text</th>
                <th>Image</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["contenus"]) || array_key_exists("contenus", $context) ? $context["contenus"] : (function () { throw new RuntimeError('Variable "contenus" does not exist.', 23, $this->source); })()));
        $context['_iterated'] = false;
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["contenu"]) {
            // line 24
            echo "            <tr>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contenu"], "id", [], "any", false, false, false, 25), "html", null, true);
            echo "</td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contenu"], "text", [], "any", false, false, false, 26), "html", null, true);
            echo "</td>
                <td><img src=\"";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contenu"], "image", [], "any", false, false, false, 27), "html", null, true);
            echo "\" alt=\"photo du contenu\" width=\"300px\" height=\"200px\"></td>
                <td>
                    <a class=\"btn btn-warning btn-sm\" href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_contenu_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["contenu"], "id", [], "any", false, false, false, 29)]), "html", null, true);
            echo "\">Éditer</a>
                    ";
            // line 30
            echo twig_include($this->env, $context, "contenu/_delete_form.html.twig");
            echo "
                </td>
            </tr>
        ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 34
            echo "            <tr>
                <td colspan=\"4\">Auncun contenu présent dans la base de donnée</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contenu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "        </tbody>
        </table>
    </div>

        <div class=\"container-full d-flex justify-content-center\">
             <a class=\"btn btn-dark mb-4\" href=\"";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_contenu_new");
        echo "\">Nouveau contenu</a>
        </div>
    ";
        // line 45
        echo twig_include($this->env, $context, "footer/footer.html.twig");
        echo "
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    public function getTemplateName()
    {
        return "contenu/index_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 45,  164 => 43,  157 => 38,  148 => 34,  131 => 30,  127 => 29,  122 => 27,  118 => 26,  114 => 25,  111 => 24,  93 => 23,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Contenu index{% endblock %}

{% block body %}
{{ include('header_admin/header_admin.html.twig') }}

    <div class=\"container-full text-center\">
        <h2 class=\"mt-4\">Contenu index</h2>
    </div>

    <div class=\"container\">
        <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Text</th>
                <th>Image</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for contenu in contenus %}
            <tr>
                <td>{{ contenu.id }}</td>
                <td>{{ contenu.text }}</td>
                <td><img src=\"{{ contenu.image }}\" alt=\"photo du contenu\" width=\"300px\" height=\"200px\"></td>
                <td>
                    <a class=\"btn btn-warning btn-sm\" href=\"{{ path('app_contenu_edit', {'id': contenu.id}) }}\">Éditer</a>
                    {{ include('contenu/_delete_form.html.twig') }}
                </td>
            </tr>
        {% else %}
            <tr>
                <td colspan=\"4\">Auncun contenu présent dans la base de donnée</td>
            </tr>
        {% endfor %}
        </tbody>
        </table>
    </div>

        <div class=\"container-full d-flex justify-content-center\">
             <a class=\"btn btn-dark mb-4\" href=\"{{ path('app_contenu_new') }}\">Nouveau contenu</a>
        </div>
    {{ include('footer/footer.html.twig') }}
{% endblock %}
", "contenu/index_admin.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/contenu/index_admin.html.twig");
    }
}
