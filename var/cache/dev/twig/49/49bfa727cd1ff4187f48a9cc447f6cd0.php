<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* coordonnee/index.html.twig */
class __TwigTemplate_56361a6cc6f61a76af0185ec78e3f957 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "coordonnee/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "coordonnee/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "coordonnee/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "DeadCowDiner - Coordonnées";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "<link rel=\"stylesheet\" href=\"/styles/coordonnee.css\">
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 10
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 11
        echo twig_include($this->env, $context, "header/header.html.twig");
        echo "
<main>
    <div class=\"container text-center mt-5\">
        <h2 class=\"mt-1\">Nous contactez</h2>
    </div>
    <div class=\"container mt-2 mb-5\">
        <div class=\"row\">
            <div class=\"col-6\">
                <div class=\"d-flex flex-column justify-content-center\">
                    <p class=\"mt-2\">Téléphone : <a class=\"text-tel\" href=\"tel:+0622334455\">06 22 33 44 55</a></p>
                    <p>30 Avenue du Dr Paul Pompidor</p>
                    <p>11100, Narbonne</p>
                    <p>Adresse e-mail : deadcowdinner@gmail.com</p>
                    <p>Ouvert du mardi au dimanche, de 12h à 14h et de 19h à 22h</p>
                </div>
                 <img class=\"img-restaurant\" src=\"/images/restaurant.jpeg\" alt=\"Notre restaurant\">
            </div>
            <div class=\"col-6\">
                <div class=\"map-responsive col-md-12\">
                    <iframe class=\"mt-2\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2909.3124807223503!2d2.9766417153972977!3d43.18195379079293!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b1ac52defc6925%3A0xb10f76c5fd1d874a!2sIN&#39;ESS%20Le%20Grand%20Narbonne!5e0!3m2!1sfr!2sfr!4v1646334812467!5m2!1sfr!2sfr\" width=\"600\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\"></iframe>
                </div>
            </div>
        </div>

        <div class=\"row\">
            <div class=\"col-2 d-flex\">
            <a class=\"btn btn-dark mt-2 mb-2\" href=\"/\">Accueil</a>
            <a class=\"btn btn-danger mt-2 mb-2 ms-2\" href=\"/carte/\">Notre carte</a>
            </div>
        </div>
    </div>
</main>
";
        // line 43
        echo twig_include($this->env, $context, "footer/footer.html.twig");
        echo "
";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "coordonnee/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 43,  110 => 11,  100 => 10,  89 => 7,  79 => 6,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}DeadCowDiner - Coordonnées{% endblock %}


{% block stylesheets %}
<link rel=\"stylesheet\" href=\"/styles/coordonnee.css\">
{% endblock %}

{% block body %}
{{ include('header/header.html.twig') }}
<main>
    <div class=\"container text-center mt-5\">
        <h2 class=\"mt-1\">Nous contactez</h2>
    </div>
    <div class=\"container mt-2 mb-5\">
        <div class=\"row\">
            <div class=\"col-6\">
                <div class=\"d-flex flex-column justify-content-center\">
                    <p class=\"mt-2\">Téléphone : <a class=\"text-tel\" href=\"tel:+0622334455\">06 22 33 44 55</a></p>
                    <p>30 Avenue du Dr Paul Pompidor</p>
                    <p>11100, Narbonne</p>
                    <p>Adresse e-mail : deadcowdinner@gmail.com</p>
                    <p>Ouvert du mardi au dimanche, de 12h à 14h et de 19h à 22h</p>
                </div>
                 <img class=\"img-restaurant\" src=\"/images/restaurant.jpeg\" alt=\"Notre restaurant\">
            </div>
            <div class=\"col-6\">
                <div class=\"map-responsive col-md-12\">
                    <iframe class=\"mt-2\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2909.3124807223503!2d2.9766417153972977!3d43.18195379079293!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b1ac52defc6925%3A0xb10f76c5fd1d874a!2sIN&#39;ESS%20Le%20Grand%20Narbonne!5e0!3m2!1sfr!2sfr!4v1646334812467!5m2!1sfr!2sfr\" width=\"600\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\"></iframe>
                </div>
            </div>
        </div>

        <div class=\"row\">
            <div class=\"col-2 d-flex\">
            <a class=\"btn btn-dark mt-2 mb-2\" href=\"/\">Accueil</a>
            <a class=\"btn btn-danger mt-2 mb-2 ms-2\" href=\"/carte/\">Notre carte</a>
            </div>
        </div>
    </div>
</main>
{{ include('footer/footer.html.twig') }}
{% endblock %}
", "coordonnee/index.html.twig", "/Users/leo.canet/Desktop/DeadCowDiner_Sym/DeadCowDiner_Sym/templates/coordonnee/index.html.twig");
    }
}
