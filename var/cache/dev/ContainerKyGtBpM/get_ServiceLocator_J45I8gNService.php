<?php

namespace ContainerKyGtBpM;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_J45I8gNService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.j45I8gN' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.j45I8gN'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'contenuRepository' => ['privates', 'App\\Repository\\ContenuRepository', 'getContenuRepositoryService', true],
        ], [
            'contenuRepository' => 'App\\Repository\\ContenuRepository',
        ]);
    }
}
