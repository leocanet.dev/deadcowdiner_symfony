<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'app_contact')]
    public function index(Request $request, MailerInterface $mailer): Response
    {
        $form = $this->createForm(ContactType::class);
        
        $contact = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // On crée le mail
            $email = (new TemplatedEmail())
            ->from($contact->get('email')->getData())
            ->to('wosokeg834@ketchet.com')
            ->subject('Avis à propos de DeadCowDiner')
            ->htmlTemplate('contact/contact.html.twig')
            ->context([
                'mail' => $contact->get('email')->getData(),
                'message' => $contact->get('message')->getData(),
                'title' => 'Avis à propos de DeadCowDiner'
            ]);
            
            // On envoie le mail
            $mailer-> send($email);
            
            // On redirige
            $this->addFlash('message', 'Votre e-mail a bien été envoyé');
            $this->redirectToRoute('app_produit_index');
        }

        return $this->render('contact/index.html.twig', [
            'controller_name' => 'ContactController',
            'form' => $form->createView()
        ]);
    }
}