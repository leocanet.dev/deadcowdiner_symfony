<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoordonneeController extends AbstractController
{
    #[Route('/coordonnee', name: 'app_coordonnee')]
    public function index(): Response
    {
        return $this->render('coordonnee/index.html.twig', [
            'controller_name' => 'CoordonneeController',
        ]);
    }
}