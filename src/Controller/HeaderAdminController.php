<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HeaderAdminController extends AbstractController
{
    #[Route('/header/admin', name: 'app_header_admin')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        
        return $this->render('header_admin/index.html.twig', [
            'controller_name' => 'HeaderAdminController',
        ]);
    }
}