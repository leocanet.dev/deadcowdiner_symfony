function getNavResponsive() {
  let nav = document.getElementById("navbar");
  switch (true) {
    case nav.classList.contains("nav") && !nav.classList.contains("responsive"):
      nav.classList.add("responsive");
      nav.classList.remove("nav-sticky");
      break;
    case nav.classList.contains("responsive"):
      nav.classList.remove("responsive");
      break;
  }
}
