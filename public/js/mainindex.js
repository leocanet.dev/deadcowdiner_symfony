// Déclaration des variables pour initialiser le jour, l'heure et les minutes.
// Weekdays est un tableau qui contient les horaires du restaurant selon les jours de la semaine
let d = new Date();
let n = d.getDay();
let now = d.getHours() + "." + d.getMinutes();
let weekdays = [
  ["Sunday", 12.0, 14.0, 19.0, 22.0],
  ["Monday"], // Restaurant fermé le lundi
  ["Tuesday", 12.0, 14.0, 19.0, 22.0],
  ["Wednesday", 12.0, 14.0, 19.0, 22.0],
  ["Thursday", 12.0, 14.0, 19.0, 22.0],
  ["Friday", 12.0, 14.0, 19.0, 22.0],
  ["Saturday", 12.0, 14.0, 19.0, 22.0],
];
let day = weekdays[n];

// Verifie la date et l'heure selon les horaires du restaurant
if ((now > day[1] && now < day[2]) || (now > day[3] && now < day[4])) {
  document.getElementById("currently_open").innerHTML = " Restaurant Ouvert";
  document.getElementById("open").classList.remove("close");
} else {
  document.getElementById("currently_open").innerHTML = " Restaurant Fermé";
  document.getElementById("open").classList.add("close");
  document.getElementById("currently_open").classList.add("currently_closed");
}
